﻿using AccurateTuner.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace AccurateTuner
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public TunerViewModel ViewModel;

        // Constructor
        public MainPage()
        {
            InitializeComponent();
            ViewModel = new TunerViewModel(Dispatcher);
            ViewModel.PropertyChanged += ViewModel_PropertyChanged;
            DataContext = ViewModel;
        }

        void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("SelectedIndex"))
            {
                SelectionBorder.Width = (LineContainer.ActualWidth / ViewModel.MaxHistory);
                SelectionTransform.TranslateX = ViewModel.SelectedIndex * (LineContainer.ActualWidth / ViewModel.MaxHistory);
            }
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            if(e.NavigationMode == NavigationMode.Back)
            {
                ViewModel.PropertyChanged -= ViewModel_PropertyChanged;
                ViewModel.Dispose();
            }
        }

        /*protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            if (FilterMenu.Visibility == System.Windows.Visibility.Visible)
            {
                FilterMenu.Visibility = System.Windows.Visibility.Collapsed;
                FilterMenuButton.Visibility = System.Windows.Visibility.Visible;
                e.Cancel = true;
            }
            base.OnBackKeyPress(e);
        }*/

        /*private void GenerateLine(List<WaveElement> frequensies, double maxAmplitude)
        {
            double yFactor = LineContainer.ActualHeight / maxAmplitude;
            PathFigure line = new PathFigure();
            line.Segments = new PathSegmentCollection();

            double xStep = LineContainer.ActualWidth / frequensies.Count;

            line.StartPoint = new Point(0, LineContainer.ActualHeight);
            for (int n = 0, samples = 0; n < frequensies.Count; n++, samples++)
            {
                line.Segments.Add(new LineSegment() { Point = new Point(n * xStep, LineContainer.ActualHeight - frequensies[n].Amplitude * yFactor) });
            }

            LineFigures.Clear();
            LineFigures.Add(line);

            if (!capturer.IsRecording)
            {
                WaveElements.ItemsSource = analyzer.GetMainElements(frequensies, 5);
            }
            else
            {
                WaveElement mainTone = analyzer.GetMainElements(frequensies, 1).FirstOrDefault();
                if(mainTone != null)
                    MainToneText.Text = string.Format("{0}, diff = {1:0.00}Hz", mainTone.WellTemperatedNote, mainTone.WellTemperatedDiff);
            }
        }*/

        private void RecordButton_Tap(object sender, TappedRoutedEventArgs e)
        {
            ViewModel.Record();

            FilterMenu.Visibility = Visibility.Collapsed;
            FilterMenuButton.Visibility = ViewModel.IsRecording ? Visibility.Collapsed : Visibility.Visible;
            RecordButton.Content = ViewModel.IsRecording ? "Stop" : "Record";
            WaveElements.Visibility = ViewModel.IsRecording ? Visibility.Collapsed : Visibility.Visible;
            MainToneText.Visibility = ViewModel.IsRecording ? Visibility.Visible : Visibility.Collapsed;
        }

        private void TimerRecordButton_Tap(object sender, TappedRoutedEventArgs e)
        {
            ViewModel.TimerRecord();
        }

        private void LowRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                int filter;
                bool success = int.TryParse((sender as FrameworkElement).Tag.ToString(), out filter);
                if (success)
                    ViewModel.SetLowFilter(filter);
            }
            catch (Exception) { }

            if (FilterMenuButton != null)
            {
                FilterMenuButton.Visibility = Visibility.Visible;
                FilterMenu.Visibility = Visibility.Collapsed;
            }
        }

        private void HighRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                int filter = int.MaxValue;
                object tag = (sender as RadioButton).Tag;
                bool success = int.TryParse((sender as RadioButton).Tag.ToString(), out filter);
                if (success)
                    ViewModel.SetHighFilter(filter);
            }
            catch (Exception) { }

            if (FilterMenuButton != null)
            {
                FilterMenuButton.Visibility = Visibility.Visible;
                FilterMenu.Visibility = Visibility.Collapsed;
            }
        }

        private void FilterMenuButton_Tap(object sender, TappedRoutedEventArgs e)
        {
            FilterMenuButton.Visibility = Visibility.Collapsed;
            FilterMenu.Visibility = Visibility.Visible;
        }

        private void SetSelection(double xPos)
        {
            int index = (int)(xPos / (LineContainer.ActualWidth / ViewModel.MaxHistory));
            index = Math.Max(0, Math.Min(index, ViewModel.FrequecyHistory.Count - 1));
            ViewModel.SelectedIndex = index;
        }

        private void LineContainer_PointerMoved(object sender, PointerRoutedEventArgs e)
        {
            if (!ViewModel.IsRecording && !ViewModel.IsRecording)
            {
                SetSelection(e.GetCurrentPoint(sender as UIElement).Position.X);
            }
        }

        private void LineContainer_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            if (!ViewModel.IsRecording && !ViewModel.IsRecording)
            {
                SetSelection(e.GetCurrentPoint(sender as UIElement).Position.X);
            }
        }
    }
}
