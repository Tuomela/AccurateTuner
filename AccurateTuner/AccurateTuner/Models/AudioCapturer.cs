﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Media;
using Windows.Media.Audio;
using Windows.Media.Capture;
using Windows.Media.Render;
using Windows.UI.Popups;

namespace AccurateTuner.Models
{
    public class AudioCapturer : IDisposable
    {
        public int SampleLength { get; set; }

        public int SampleRate
        {
            get { return graph != null ? (int)graph.EncodingProperties.SampleRate : 0; }
        }

        public bool IsRecording { get; private set; }

        private AudioGraph graph;
        private AudioDeviceInputNode inputNode;
        private AudioFrameOutputNode outputNode;

        public event EventHandler<byte[]> DataReceived;

        public AudioCapturer(int sampleLength)
        {
            SampleLength = sampleLength;
            IsRecording = false;
        }

        public async void Initialize()
        {
            try
            {
                // Create graph
                AudioGraphSettings settings = new AudioGraphSettings(AudioRenderCategory.Media);
                settings.DesiredSamplesPerQuantum = SampleLength;
                settings.DesiredRenderDeviceAudioProcessing = Windows.Media.AudioProcessing.Default;
                settings.QuantumSizeSelectionMode = QuantumSizeSelectionMode.ClosestToDesired;

                CreateAudioGraphResult result = await AudioGraph.CreateAsync(settings);
                if (result.Status != AudioGraphCreationStatus.Success)
                {
                    // Cannot create graph
                    ShowError("Cannot create audio graph!");
                    return;
                }
                graph = result.Graph;

                // Create a device input node using the default audio input device
                CreateAudioDeviceInputNodeResult deviceInputNodeResult = await graph.CreateDeviceInputNodeAsync(MediaCategory.Other);

                if (deviceInputNodeResult.Status != AudioDeviceNodeCreationStatus.Success)
                {
                    ShowError("Cannot create device input node!");
                    return;
                }

                inputNode = deviceInputNodeResult.DeviceInputNode;

                outputNode = graph.CreateFrameOutputNode();
                graph.QuantumProcessed += AudioGraph_QuantumProcessed;

                // Because we are using lowest latency setting, we need to handle device disconnection errors
                graph.UnrecoverableErrorOccurred += Graph_UnrecoverableErrorOccurred;
            }
            catch (Exception e)
            {
                ShowError(e.ToString());
            }
        }

        public void StartRecording()
        {
            outputNode.Start();
            graph.Start();
            IsRecording = true;
        }

        public void StopRecording()
        {
            outputNode.Stop();
            graph.Stop();
            IsRecording = false;
        }

        private void Graph_UnrecoverableErrorOccurred(AudioGraph sender, AudioGraphUnrecoverableErrorOccurredEventArgs args)
        {
            StopRecording();
            ShowError(args.Error.ToString());
        }

        private void AudioGraph_QuantumProcessed(AudioGraph sender, object args)
        {
            AudioFrame frame = outputNode.GetFrame();
            ProcessFrameOutput(frame);
        }

        unsafe private void ProcessFrameOutput(AudioFrame frame)
        {
            using (AudioBuffer buffer = frame.LockBuffer(AudioBufferAccessMode.Write))
            using (IMemoryBufferReference reference = buffer.CreateReference())
            {
                byte* dataInBytes;
                uint capacityInBytes;

                // Get the buffer from the AudioFrame
                ((IMemoryBufferByteAccess)reference).GetBuffer(out dataInBytes, out capacityInBytes);

                byte[] output = new byte[capacityInBytes];

                for(int n=0; n<capacityInBytes; n++)
                {
                    output[n] = dataInBytes[n];
                }

                if(DataReceived != null)
                {
                    DataReceived(this, output);
                }
            }
        }

        private async void ShowError(string message)
        {
            await new MessageDialog(message).ShowAsync();
        }

        public void Dispose()
        {
            StopRecording();
            graph.Dispose();
            outputNode.Dispose();
            inputNode.Dispose();
        }
    }
}
