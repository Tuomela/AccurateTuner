﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace AccurateTuner.Models
{
    public class SoundAnalyzer
    {
        private double A4Freq;
        private int SampleRate;

        private List<double> noteFrequensies;
        private List<string> notes;
        public static List<string> NoteNames = new List<string>() { "A", "A#", "H", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#" };

        public SoundAnalyzer(int sampleRate, double a4Freq = 440)
        {
            A4Freq = a4Freq;
            SampleRate = sampleRate;

            notes = new List<string>();
            noteFrequensies = new List<double>();

            int octave = 0;
            for (int a = 0; a < 10; a++)
            {
                double aFreq = a4Freq / (Math.Pow(2, 4 - a));
                for (int n = 0; n < 12; n++)
                {
                    double noteFreq = Math.Pow(1.059463094359, n) * aFreq;
                    string noteChar = NoteNames[n % 12];
                    if (noteChar.Equals("C"))
                        octave++;
                    string note = noteChar + octave;

                    noteFrequensies.Add(noteFreq);
                    notes.Add(note);
                }
            }
        }

        public async Task<List<WaveElement>> GetFrequensies(byte[] data, int precicion)
        {
            return await Task.Run(() =>
            {
                Complex[] transformed = Fourier.FFT(BytesToComplexList(data, data.Length / precicion));

                List<WaveElement> frequensies = new List<WaveElement>(transformed.Length);

                double factor = (double)SampleRate / transformed.Length;
                double amplitudeFactor = 1 / Math.Sqrt(transformed.Length);

                for (int n = 10; n < transformed.Length / 2 - 1; n++)
                {
                    double freq = n * factor;
                    double amplitude = transformed[n].Magnitude * amplitudeFactor;
                    frequensies.Add(new WaveElement() { Amplitude = amplitude, Frequency = freq });
                }

                return frequensies;
            });
        }

        private Complex[] BytesToComplexList(byte[] bytes, int count)
        {
            Complex[] result = new Complex[Math.Min(bytes.Length, count)];
            for (int n = 0; n < bytes.Length && n < count; n++)
            {
                result[n] = new Complex((int)bytes[n], 0);
            }
            return result;
        }

        private byte[] GetDataIn(byte[] data, int precicion)
        {
            if (precicion == 1)
                return data;

            byte[] result = new byte[data.Length / precicion];
            int block = 0;
            byte sum = 0;
            for (int n = 0; n < data.Length; n++)
            {
                sum += data[n];
                if (n % precicion == 0 && n > 0)
                {
                    byte avg = (byte)(sum / precicion);
                    result[block] = avg;
                    block++;
                }
            }
            return result;
        }

        public int GetNearestNoteIndex(double frequency)
        {
            int minIndex = -1;
            double minDistance = 0;
            for (int i = 0; i < noteFrequensies.Count; i++)
            {
                var distance = Math.Abs(frequency - noteFrequensies[i]);
                if (minIndex == -1 || distance < minDistance)
                {
                    minDistance = distance;
                    minIndex = i;
                }
                else if (minIndex > 0)
                {
                    break;
                }
            }

            return minIndex;
        }

        internal string GetNoteName(int noteIndex)
        {
            return notes[noteIndex];
        }

        internal double GetNoteFreq(int noteIndex)
        {
            return noteFrequensies[noteIndex];
        }

        public List<WaveElement> GetMainElements(List<WaveElement> allElements, int num)
        {
            List<WaveElement> elements = new List<WaveElement>();
            var ordered = allElements.OrderByDescending(f => f.Amplitude);
            for (int n = 0; n < num * 5; n++)
            {
                WaveElement element = ordered.ElementAt(n);
                int noteIndex = GetNearestNoteIndex(element.Frequency);
                element.WellTemperatedNote = GetNoteName(noteIndex);
                element.WellTemperatedDiff = element.Frequency - GetNoteFreq(noteIndex);

                WaveElement neighbour = elements.Where(f => Math.Abs(GetNearestNoteIndex(f.Frequency) - noteIndex) < 2).FirstOrDefault();
                if (neighbour != null)
                {
                    double nF = neighbour.Amplitude / (neighbour.Amplitude + element.Amplitude);
                    double eF = element.Amplitude / (element.Amplitude + neighbour.Amplitude);
                    neighbour.Frequency = neighbour.Frequency * nF + element.Frequency * eF;
                    neighbour.Amplitude = nF * neighbour.Amplitude + eF * element.Amplitude;
                    int newIndex = GetNearestNoteIndex(neighbour.Frequency);
                    neighbour.WellTemperatedNote = GetNoteName(newIndex);
                    neighbour.WellTemperatedDiff = neighbour.Frequency - GetNoteFreq(newIndex);
                }
                else
                {
                    if (elements.Count >= num) break;

                    if (n == 0 ||
                       (element.Frequency > elements[0].Frequency || element.Amplitude / elements[0].Amplitude > 0.8) ||
                       element.WellTemperatedNote.Equals(elements[0].WellTemperatedNote))
                        elements.Add(element);
                }
            }

            return elements.OrderByDescending(f => f.Frequency).ToList();
        }
    }
}
