﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Media.Capture;
using Windows.Media.MediaProperties;
using Windows.Storage.Streams;
using Windows.UI.Popups;
using Windows.UI.Xaml;

namespace AccurateTuner.Models
{
    public class WaveCapturer : IDisposable
    {
        public event EventHandler<byte[]> SoundCaptured;

        public bool IsRecording { get; private set; }

        public int SampleRate
        {
            get
            {
                return 44100;
            }
        }

        MediaCapture Capturer;
        DispatcherTimer dt;
        InMemoryRandomAccessStream buffer;

        public WaveCapturer()
        {
            dt = new DispatcherTimer();
            dt.Interval = TimeSpan.FromMilliseconds(25);
            //dt.Start();

            //Capturer.
        }

        public void Dispose()
        {
            StopRecording();
            if(buffer != null)
                buffer.Dispose();
        }

        private async Task<bool> RecordProcess()
        {
            if (buffer != null)
            {
                buffer.Dispose();
            }
            buffer = new InMemoryRandomAccessStream();
            if (Capturer != null)
            {
                Capturer.Dispose();
            }
            try
            {
                MediaCaptureInitializationSettings settings = new MediaCaptureInitializationSettings
                {
                    StreamingCaptureMode = StreamingCaptureMode.Audio
                };
                Capturer = new MediaCapture();
                await Capturer.InitializeAsync(settings);
                Capturer.RecordLimitationExceeded += async (MediaCapture sender) =>
                {
                    //Stop 
                    await Capturer.StopRecordAsync(); 
                    IsRecording = false;
                    ShowError("Record Limitation Exceeded ");
                };
                Capturer.Failed += (MediaCapture sender, MediaCaptureFailedEventArgs errorEventArgs) =>
                {
                    IsRecording = false;
                    ShowError(string.Format("Code: {0}. {1}", errorEventArgs.Code, errorEventArgs.Message));
                };
            }
            catch (Exception ex)
            {
                IsRecording = false;
                if (ex.InnerException != null && ex.InnerException.GetType() == typeof(UnauthorizedAccessException))
                {
                    ShowError(ex.InnerException.Message);
                }
                return false;
            }
            return true;
        }

        public async void StartRecording()
        {
            if (!IsRecording)
            {
                IsRecording = true;
                await RecordProcess();
                await Capturer.StartRecordToStreamAsync(MediaEncodingProfile.CreateWav(AudioEncodingQuality.Low), buffer);
            }
        }

        public async void StopRecording()
        {
            IsRecording = false;
            if (Capturer != null)
            {
                await Capturer.StopRecordAsync();
                Capturer.Dispose();
            }
        }

        private async void ShowError(string message)
        {
            await new MessageDialog(message).ShowAsync();
        }
    }
}
