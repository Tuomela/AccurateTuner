﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccurateTuner.Models
{
    public class WaveElement
    {
        public double Frequency { get; set; }
        public double Amplitude { get; set; }
        public string WellTemperatedNote { get; set; }
        public double WellTemperatedDiff { get; set; }

        public string Info
        {
            get
            {
                return ToString();
            }
        }

        public override string ToString()
        {
            return string.Format("{0}: (f = {1:0.00}Hz, diff = {2:0.00}Hz, a={3:0.00})", WellTemperatedNote, Frequency, WellTemperatedDiff, Amplitude);
        }
    }
}
