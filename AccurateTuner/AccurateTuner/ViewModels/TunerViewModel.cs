﻿using AccurateTuner.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media.Imaging;

namespace AccurateTuner.ViewModels
{
    public class TunerViewModel : INotifyPropertyChanged, IDisposable
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string value)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(value));
            }
        }

        private WriteableBitmap _frequencyHistoryImage;
        public WriteableBitmap FrequencyHistoryImage
        {
            get
            {
                return _frequencyHistoryImage;
            }
            set
            {
                if (_frequencyHistoryImage != value)
                {
                    _frequencyHistoryImage = value;
                    NotifyPropertyChanged("FrequencyHistoryImage");
                }
            }
        }

        private List<List<WaveElement>> _freqHistory;
        public List<List<WaveElement>> FrequecyHistory
        {
            get
            {
                return _freqHistory;
            }
            set
            {
                if (_freqHistory != value)
                {
                    _freqHistory = value;
                    NotifyPropertyChanged("FrequecyHistory");
                }
            }
        }

        private List<byte[]> _dataHistory;
        public List<byte[]> DataHistory
        {
            get
            {
                return _dataHistory;
            }
            set
            {
                if (_dataHistory != value)
                {
                    _dataHistory = value;
                    NotifyPropertyChanged("DataHistory");
                }
            }
        }

        private List<WaveElement> _lastFreqs;
        public List<WaveElement> CurrentMainFrequencies
        {
            get
            {
                return _lastFreqs;
            }
            set
            {
                if (_lastFreqs != value)
                {
                    _lastFreqs = value;
                    MainToneText = _lastFreqs.FirstOrDefault().Info;
                    NotifyPropertyChanged("CurrentMainFrequencies");
                }
            }
        }

        private int _selectedIndex;
        public int SelectedIndex
        {
            get
            {
                return _selectedIndex;
            }
            set
            {
                if (_selectedIndex != value)
                {
                    _selectedIndex = value;
                    if (!IsRecording && DataHistory.Count > 0)
                        RecalculateBuffer(value);
                    NotifyPropertyChanged("SelectedIndex");
                }
            }
        }

        private string _mainTone;
        public string MainToneText
        {
            get
            {
                return _mainTone;
            }
            set
            {
                if (_mainTone != value)
                {
                    _mainTone = value;
                    NotifyPropertyChanged("MainToneText");
                }
            }
        }

        public bool IsRecording
        {
            get
            {
                return capturer.IsRecording;
            }
        }

        public int MaxHistory
        {
            get { return _maxHistory; }
        }

        public double MaxAmplitude { get; set; }

        public double LowFilter = 40;
        public double HighFilter = double.MaxValue;

        AudioCapturer capturer;
        SoundAnalyzer analyzer;

        //byte[] lastBuffer;

        DispatcherTimer timer;
        CoreDispatcher _dispathcer;
        private const int _maxHistory = 15;
        private const int _imageHeight = 300;

        public TunerViewModel(CoreDispatcher dispatcher)
        {
            capturer = new AudioCapturer(2048);
            capturer.Initialize();
            capturer.DataReceived += Capturer_DataReceived;
            analyzer = new SoundAnalyzer(capturer.SampleRate);
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(5);
            timer.Tick += TimerRecordStop;

            FrequecyHistory = new List<List<WaveElement>>();
            DataHistory = new List<byte[]>();

            _dispathcer = dispatcher;
        }

        private void Capturer_DataReceived(object sender, byte[] e)
        {
            //lastBuffer = e;
            byte[] temp = new byte[e.Length];
            Array.Copy(e, temp, e.Length);
            DataHistory.Add(temp);
            if (DataHistory.Count > MaxHistory)
            {
                DataHistory.RemoveAt(0);
            }
            AnalyzeLastBuffer();
        }

        public void SetLowFilter(int filter)
        {
            LowFilter = filter;
        }

        public void SetHighFilter(int filter)
        {
            HighFilter = filter;
        }

        public void Record()
        {
            if (!capturer.IsRecording)
            {
                MaxAmplitude = 1;
                FrequecyHistory.Clear();
                DataHistory.Clear();
                SelectedIndex = 0;
                FrequencyHistoryImage = null;
                capturer.StartRecording();
            }
            else
            {
                capturer.StopRecording();
                RecalculateBuffer(SelectedIndex);
            }

            NotifyPropertyChanged("IsRecording");
        }

        public void TimerRecord()
        {
            if (!capturer.IsRecording)
            {
                Record();
                timer.Start();
            }
        }

        public void Dispose()
        {
            if (capturer != null)
            {
                capturer.DataReceived -= Capturer_DataReceived;
                capturer.Dispose();
            }
        }

        private void TimerRecordStop(object sender, object e)
        {
            timer.Stop();
            if (capturer.IsRecording)
                Record();
        }

        private void capturer_SoundCaptured(object sender, byte[] e)
        {
            //lastBuffer = e;
            byte[] temp = new byte[e.Length];
            Array.Copy(e, temp, e.Length);
            DataHistory.Add(temp);
            if (DataHistory.Count > MaxHistory)
            {
                DataHistory.RemoveAt(0);
            }
            AnalyzeLastBuffer();
        }

        private async void AnalyzeLastBuffer()
        {
            List<WaveElement> frequensies = await GetFrequencies(DataHistory.Count - 1, 8);
            FrequecyHistory.Add(frequensies);
            double maxA = frequensies.Max(f => f.Amplitude);
            MaxAmplitude = Math.Max(maxA, MaxAmplitude);

            if (FrequecyHistory.Count > MaxHistory)
            {
                FrequecyHistory.RemoveAt(0);
            }

            await _dispathcer.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                if (IsRecording)
                    SelectedIndex = FrequecyHistory.Count - 1;

                CurrentMainFrequencies = analyzer.GetMainElements(frequensies, 3);

                ConstructImage();
            });
        }

        private async Task<List<WaveElement>> GetFrequencies(int index, int precicion = 1)
        {
            List<WaveElement> frequensies = await analyzer.GetFrequensies(DataHistory[index], precicion);
            frequensies = frequensies.Where(f => f.Frequency > LowFilter && f.Frequency < HighFilter).ToList();

            return frequensies;
        }

        private async void RecalculateBuffer(int index)
        {
            List<WaveElement> frequensies = await GetFrequencies(index, 1);
            FrequecyHistory[index] = frequensies;
            CurrentMainFrequencies = analyzer.GetMainElements(frequensies, 5);

            await _dispathcer.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                ConstructImage();
            });
        }

        private void ConstructImage()
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();

            if (FrequecyHistory != null && FrequecyHistory.Count > 0)
            {
                if (FrequencyHistoryImage == null || FrequecyHistory.LastOrDefault().Count != FrequencyHistoryImage.PixelHeight)
                {
                    FrequencyHistoryImage = new WriteableBitmap(MaxHistory, FrequecyHistory.LastOrDefault().Count);
                }

                double maxFreq = FrequecyHistory.LastOrDefault().LastOrDefault().Frequency;
                double minFreq = FrequecyHistory.LastOrDefault().FirstOrDefault().Frequency;

                UInt32[] pixels = new UInt32[FrequencyHistoryImage.PixelWidth * FrequencyHistoryImage.PixelHeight];
                for (int n = 0; n < pixels.Length; n++)
                {
                    int xIndex = n % FrequencyHistoryImage.PixelWidth;
                    int yIndex = FrequencyHistoryImage.PixelHeight - 1 - (n / FrequencyHistoryImage.PixelWidth);

                    if (xIndex < FrequecyHistory.Count)
                    {
                        double yFactor = (double)FrequecyHistory[xIndex].Count / FrequencyHistoryImage.PixelHeight;
                        int yListIndex = (int)(yIndex * yFactor);

                        if (yListIndex < FrequecyHistory[xIndex].Count)
                        {
                            float volume = (float)(FrequecyHistory[xIndex][yListIndex].Amplitude / MaxAmplitude);

                            unchecked
                            {
                                float red = Math.Min(volume * volume, 1);
                                float green = Math.Min(volume * 0.9f, 0.9f);
                                float blue = Math.Min((float)Math.Sqrt(volume) / 2, 0.5f);
                                UInt32 pixel = UInt32.Parse("0xffeeaaff");
                                pixels[n] = pixel;
                            }
                        }
                    }
                }
                FrequencyHistoryImage.Invalidate();
            }

            NotifyPropertyChanged("FrequencyHistoryImage");

            watch.Stop();
            Debug.WriteLine("Drawed image. Elapsed " + watch.ElapsedMilliseconds);
        }
    }
}
